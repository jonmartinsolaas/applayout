package com.martinsolaas.spring;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import javax.annotation.PostConstruct;

@UIScope
@SpringComponent
@Route(value = ProfileView.ROUTE, layout = MainView.class)
public class ProfileView extends VerticalLayout {

    public final static String ROUTE = "profile";

    @PostConstruct
    private void init() {

        add(new DonutChartExample());
    }
}
