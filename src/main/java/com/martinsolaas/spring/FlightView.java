package com.martinsolaas.spring;

import com.github.appreciated.card.Card;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import javax.annotation.PostConstruct;

@UIScope
@Route(value = FlightView.ROUTE, layout = MainView.class)
@SpringComponent
public class FlightView extends VerticalLayout {

    public final static String ROUTE = "";

    @PostConstruct
    private void init() {
        add(new FlexibleGridLayoutExample());
        add(new Card());
    }

}
