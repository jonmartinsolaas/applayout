package com.martinsolaas.spring;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import javax.annotation.PostConstruct;

@UIScope
@SpringComponent
@Route(value = OtherView.ROUTE, layout = MainView.class)
public class OtherView extends VerticalLayout {

    public final static String ROUTE = "other";

    @PostConstruct
    public void init() {
        add(new AreaChartExample(), buttons());
    }

    public HorizontalLayout buttons() {
        HorizontalLayout hl = new HorizontalLayout();

        hl.add(new Button("One"), new Button("two"));
        hl.setWidthFull();
        hl.setJustifyContentMode(JustifyContentMode.END);
        return hl;
    }
}
