package com.martinsolaas.spring;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.server.PWA;

@PWA(name = "Application template", shortName = "App template")
public class AppShellConfiguratorImpl implements AppShellConfigurator {
}
