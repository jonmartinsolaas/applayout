package com.martinsolaas.spring;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;

import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;

import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.spring.annotation.UIScope;

import javax.annotation.PostConstruct;

@UIScope
public class MainView extends AppLayout {

    Tabs navbarTabs;
    Tabs drawerTabs;

    @PostConstruct
    public void init() {

        Image img = new Image(new StreamResource("logo-image.png", () ->
            MainView.class.getClassLoader().getResourceAsStream("META-INF/resources/icons/icon.png")
            ), "Logo");

        img.setHeight("44px");
        addToNavbar(true, new DrawerToggle(), img);

        navbarTabs = new Tabs(
                  new NavTab("Flightx", FlightView.class)
                , new NavTab("Trends",  TrendsView.class)
                , new NavTab("Profile",  ProfileView.class)
                , new NavTab("Other",  OtherView.class)
                , new NavTab("Log out",  StoreView.class)
        );

        drawerTabs = new Tabs(
                  new NavTab("Flightx",  FlightView.class)
                , new NavTab("Trends",  TrendsView.class)
                , new NavTab("Profile", ProfileView.class)
                , new NavTab("Other",  OtherView.class)
                , new NavTab("Log out",  StoreView.class)
        );

        navbarTabs.setOrientation(Tabs.Orientation.HORIZONTAL);
        drawerTabs.setOrientation(Tabs.Orientation.VERTICAL);
        navbarTabs.setSizeFull();

        addToNavbar(true, navbarTabs);
        addToDrawer(drawerTabs);
        this.setPrimarySection(Section.NAVBAR);
    }

    static class NavTab extends Tab implements AfterNavigationObserver {

        public final RouterLink link;

        public NavTab(String text, Class<? extends Component> navigationTarget) {
            link = new RouterLink(null, navigationTarget);
            //link.add(VaadinIcon.BULLSEYE.create());
            link.add(text);
            this.add(link);
        }

        @Override
        public void afterNavigation(AfterNavigationEvent event) {
            if (event.getLocation().getFirstSegment().equals(link.getHref()))
                ((Tabs)this.getParent().get()).setSelectedTab(this);
        }

    }
}
