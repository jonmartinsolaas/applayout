package com.martinsolaas.spring;

import com.github.appreciated.card.Card;
import com.github.appreciated.card.content.IconItem;
import com.github.appreciated.css.grid.GridLayoutComponent;
import com.github.appreciated.css.grid.sizes.Flex;
import com.github.appreciated.css.grid.sizes.Length;
import com.github.appreciated.css.grid.sizes.MinMax;
import com.github.appreciated.css.grid.sizes.Repeat;
import com.github.appreciated.layout.FlexibleGridLayout;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.StreamResource;

public class FlexibleGridLayoutExample extends HorizontalLayout {
    public FlexibleGridLayoutExample() {
        FlexibleGridLayout layout = new FlexibleGridLayout()
                .withColumns(Repeat.RepeatMode.AUTO_FILL, new MinMax(new Length("190px"), new Flex(1)))
                .withAutoRows(new Length("120px"))
                .withItems(
                          new CardExample("META-INF/resources/icons/icon.png","Title","desc")
                        , new CardExample("META-INF/resources/icons/icon.png","Title","desc")
                        , new CardExample("META-INF/resources/icons/icon.png","Title","desc")
                        , new CardExample("META-INF/resources/icons/icon.png","Title","desc")
                        , new CardExample("META-INF/resources/icons/icon.png","Title","desc")

                )
                .withPadding(false)
                .withSpacing(false)
                .withAutoFlow(GridLayoutComponent.AutoFlow.ROW_DENSE)
                .withOverflow(GridLayoutComponent.Overflow.AUTO);
        layout.setSizeFull();
        setSizeFull();
        add(layout);
    }

    public static class CardExample extends VerticalLayout {
        public CardExample(String imagePath, String title, String description) {

            Image img = new Image(new StreamResource("logo-image.png", ()
                    -> getClass().getClassLoader().getResourceAsStream(imagePath)), title);
            img.setWidth("50px");
            img.setHeight("50px");
            Card card = new Card(
                    new IconItem(img, title, description)
            );
            card.setWidth("100%");
            add(card);
        }
    }


}



