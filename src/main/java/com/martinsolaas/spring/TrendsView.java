package com.martinsolaas.spring;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import javax.annotation.PostConstruct;

@UIScope
@Route(value = TrendsView.ROUTE, layout = MainView.class)
@SpringComponent
public class TrendsView extends VerticalLayout {

    public final static String ROUTE = "trends";

    @PostConstruct
    public void init() {
        add(new Button("Trends"));
    }
}
